/**
 * PlugPlug - A script for plug.dj by Mateon1 (matin1111@wp.pl)
 * Planned:
 * - Song skiplist/banlist.
 */

/*jslint maxerr: 1000, devel: true, sloppy: true, forin: true*/


/*global API, $, jQuery, require*/

/*

// TEMPORARY FIX

var _$context = {"_events":
                 {"chat:receive":
                  [{"callback": function () {}}]
                 }
                }; // In case the require breaks, PlugPlug won't break

try {
    _$context = require("e1722/cdacb/eb26b");
} catch (e) {}

*/ // Disabling inline images for now

                                     // http://semver.org/
var version = [1, 0, 5, true],       // Bot version: [MAJOR, MINOR, PATCH, IS_PRERELEASE]
    changed = /*["+ Added more reliable history system", // RELEASE TOTAL CHANGES
               "+ Made history warnings better",
               "+ Added clickable history notifications to skip",
               "+ Added a malicious script defence mechanism",
               "* Formatted the script, JSLint gives much less errors",
               "* Fixed the script, at least temporarily",
               "* Changed the version system",
               "* Secured most regexes",
               "* Improved the suicideBomb()",
               "* Fixed inline images",
               "* Corrected the rules link in /rules",
               "* Changed all instances of .chatID and .fromID to .cid and .fid cause of the Plug API change",
               "- Deprecated rset(), no more eval(), no more potential evil",
               "- Removed most commands, not needed since we have Bot",
               "- Removed the rules, no longer needed since we have Bot",
               "- Completely removed the hackLink() function, it was broken for too long",
               "- Removed banned songs, Bot does that now",
               "* Fixed PlugPlug for new Plug",
               "- Removed stats",
               "- Removed inline images, for now"],*/
              ["* Fixed the curate notification",
               "* Fixed the curate number in the song notification"],
    canSkip = true,
    CSDelay = -1,
    lastVol = API.getVolume(),
    isMuted = !lastVol,
    artMute = false,
    isOwner = API.getUser().id === "518a0d73877b92399575657b",
    ppSaved,
    chatBan,

// ALOT   \\             //
          // alot of vars\
       //(*)  ________"(*)\
      // : ' // . var\\ ,(I love them alot!)\
     //  var//.   , ' \\  ;   .  Alots are cool
     //  .  | : var  ; | MOAR ALOT, MOAR var!!!!
    alotOfAlot = true, //A LOT OF A LOT OF ALOT
    alotOfTimeout = null; // Contains an alot  \
    //of timeout var . : alot '  " ,   \\.  ,   \
    //alot|     var "  alot ,  ' var  timeout   \
   //of leg      alot foot     "    *    much var.

Object.defineProperty(window, "history", {value: [], writable: true}); // Dirty hax :L

function getCallerName() {
    try {
        return arguments.callee.caller.caller.name || "<anonymous>";
    } catch (e) {}
}

function getCallerArgs() {
    try {
        return (/^function (?:\w+)?(\((?:(?:\w+|(?:\w+, )+\w+)?)\))/).exec(
            arguments.callee.caller.caller.toString()
        )[1];
    } catch (e) {}
    return "(<unknown>)";
}

function getVersionShort() {
    var add = "";
    if (version[3]) {
        add = "-pre";
    }
    return "v." + version[0] + "." + version[1] + "." + version[2] + add;
}

function getVersion() {
    var add = "";
    if (version[3]) {
        add = " prerelease";
    }
    return "version " + version[0] + "." + version[1] + "." + version[2] + add;
}

/*jslint bitwise: true*/
function mulStr(x, n) {
    var s = '';
    while (true) {
        if (n & 1) {s += x; }
        n >>= 1;
        if (n) {x += x; } else {break; }
    }
    return s;
}
/*jslint bitwise: false*/

/*
function terrariaDeath() {
    var result = "",
        usrs = API.getUsers();
        text = [" was slain", " was eviscerated", " was murdered", "'s face was torn off",
                "'s entrails were ripped out", " was destroyed", "'s skull was crushed", " got massacred",
                " got impaled", " was torn in half", " was decapitated", " let their arms get torn off",
                " watched their innards become outards", " was brutally dissected", "'s extremities were detached",
                "'s body was mangled", "'s vital organs were ruptured", " was turned into a pile of flesh",
                " was removed from " + worldName, " got snapped in half", " was cut down the middle", " was chopped up",
                "'s plead for death was answered", "'s meat was ripped off the bone",
                "'s flailing about was finally stopped", " had their head removed"][Math.floor(Math.random() * 26)];
    if (plr >= 0 && plr < 255) {
        result = text + " by " + usrs[~~(Math.random() * usrs.length)] + "'s " + ["magic", "shotgun", "banhammer",
                                                                                  "1337 hax"][~~(Math.random() * 12)] +
                                                                                            ".";
    } else if (npc >= 0 && Main.npc[npc].displayName != "") {
            result = text + " by " + Main.npc[npc].displayName + ".";
    } else if (proj >= 0 && Main.projectile[proj].name != "") {
        result = text + " by " + Main.projectile[proj].name + ".";
    } else if (other >= 0) {
        if (other == 0) {
            if (Main.rand.Next(2) == 0) {
                result = " fell to their death.";
            } else {
                result = " didn't bounce.";
            }
        } else if (other == 1) {
            int num2 = Main.rand.Next(4);
            if (num2 == 0) {
                result = " forgot to breathe.";
            } else if (num2 == 1) {
                result = " is sleeping with the fish.";
            } else if (num2 == 2) {
                result = " drowned.";
            } else if (num2 == 3) {
                result = " is shark food.";
            }
        } else if (other == 2) {
            int num3 = Main.rand.Next(4);
            if (num3 == 0) {
                result = " got melted.";
            } else if (num3 == 1) {
                result = " was incinerated.";
            } else if (num3 == 2) {
                result = " tried to swim in lava.";
            } else if (num3 == 3) {
                result = " likes to play in magma.";
            }
        } else if (other == 3) {
            result = text + ".";
        }
    }
    return result;
}*/

function rnd(d, m, x, u, b, a) {
    //return [0]; //lael
    // a(PI key) and b(ase) unused/unimplemented (after a month, Random.org still hasn't sent an API key)
    var r = [], up = new TypeError();
    a = a === undefined ? "[INSERT API KEY HERE]" : a;
    b = b === undefined ? 10 : b;
    if ((u && x - m > d) || d < 0) {
        throw up;
    }
    x += 1; // Include max
    if (u) {
        r = ["u", "n", "i", "m", "p", "l", "e", "m", "e", "n", "t", "e", "d"];
    } else {
        while (d > 0) {
            d -= 1;
            r.push(Math.floor(Math.random() * (x - m) + m));
        }
    }
    return r;
}

function unmute() {
    var a = isMuted;
    if (isMuted) {
        artMute = isMuted = false;
        API.setVolume(lastVol);
    }
    return a;
}

function mute() {
    var a = isMuted;
    if (!isMuted) {
        lastVol = API.getVolume();
        artMute = isMuted = true;
        API.setVolume(0);
    }
    return !a;
}

/*
(function () {
    function e() {
        t = new XMLHttpRequest();
        t.open("GET", "http://plug.dj/friendshipismagic/");
        t.onerror = e;
        t.onload = function (a) {
            if (t.status == 500)
                return e();
            console.log("Plug is working :D\nRefreshing in 3 seconds");
            setTimeout(function () {
                location.reload()
            }, 3000)
        };
        t.send()
    }
    e()
})()
*/

function cycleEnabled() {return $(".cycle-toggle")[0].className.indexOf("enabled") !== -1; }
function cycleToggle() {$(".cycle-toggle").click(); }

function appendHTML(HTML, to) {
    $("#" + to)[0].insertAdjacentHTML('beforeend', HTML);
}
function appendChat(text, style, classes, id) {
    var e = $("#chat-messages")[0],
        t = e.scrollTop + e.clientHeight === e.scrollHeight;
    appendHTML("<div "
        + (classes === undefined ? "" : "class=\"" + classes + "\" ")
        + (style === undefined ? "" : "style=\"" + style + "\" ")
        + (id === undefined ? "" : "id=\"" + id + "\"")
        + ">" + text + "</div>", "chat-messages");
    if (t) {
        e.scrollTop = e.scrollHeight;
    }
}

function getPerm(u) {
    if (u === 4187008) {return 9001; }
    if (u === "518d13ef3e083e7282a15245") {return 8001; } // Mateon2 only, Mateon3 is testing a normal user
    return API.getUser(u).role;
}
function toRole(n) {
    switch (n) {
    case 0:
        return "none";
    case 1:
        return "Resident DJ";
    case 2:
        return "Bouncer";
    case 3:
        return "Manager";
    case 4:
        return "Cohost";
    case 5:
        return "Host";
    case 8:
        return "Ambassador";
    case 10:
        return "Plug admin";
    case 9001:
        return "Mateon1";
    case 8001:
        return "Mateon's alts";
    default:
        return "undefined";
    }
}

function save() {
    localStorage.PlugPlug = JSON.stringify(ppSaved);
}
function load() {
    ppSaved = JSON.parse(localStorage.PlugPlug);
}

function get(prop) {
    return ppSaved[prop];
}
function set(prop, value) {
    ppSaved[prop] = value;
}
function rset(expr, value) {
    var add = "";
    if (getCallerName()) {
        add = " Called by function " + getCallerName() + getCallerArgs();
    }
    console.warn("[PlugPlug] rset() is deprecated, use set() instead." + add);
    eval("ppSaved" + expr + " = value;");
}

function fmtDate(e) {
//(l.m.toString().length == 1 ? "0" + l.m : l.m)
    return e.getDate() + "-" + (e.getMonth() + 1) + "-" + e.getFullYear() + " " +
           e.getHours() + ":" + (e.getMinutes().toString().length === 1 ? "0" : "") + e.getMinutes() + ":" +
                                (e.getSeconds().toString().length === 1 ? "0" : "") + e.getSeconds();
}

function log(s, p) {
    var i, o;
    s = s.replace(/&#39;/g, "'").replace(/&amp;/g, "&").replace(/&#34;/g, "\"").replace(/&#59;/g, ";").replace(/&gt;/g, ">");
    o = s.replace(/<(\w[\d\w]*)[\s\w\d\b]*>(.*?)<\/\1>/g, function (match, $1, $2, length, string) {return $2; }).replace(/&lt;/g, "<"); // Ewwwww... Parsing HTML with regex...
    i = p ? console.error(o) : console.log(o); // To calm lint down..
    /*for (i in s.split("\n")) {
        API.chatLog(s.split("\n")[i], p);
    }*/
    s = s.replace(/\n/g, "<br>");
    appendChat(
        p ?
                "<i class=\"icon icon-chat-system\"></i><span class=text>" + s + "</span>"
            :   "</i><span class=text>" + s + "</span>",
        undefined,
        p ? "system" : "update"
    );
}

function changelog() {
    var s = "New in " + getVersion() + ":\n", i;
    for (i in changed) {
        s += "\n";
        switch (changed[i][0]) {
        case "+":
            s += "<span style=\"color:#2D2\">";
            break;
        case "*":
            s += "<span style=\"color:#A7F\">";
            break;
        case "-":
            s += "<span style=\"color:#D22\">";
            break;
        default:
            s += "<span>";
        }
        s += changed[i] + "</span>";
    }
    log(s);
}
function loadMessage() {
    log("Loaded PlugPlug " + getVersion());
    if (get("lastVersion") !== version.map(function (e) {return e.toString(); }).join("-")) {
        set("lastVersion", version.map(function (e) {return e.toString(); }).join("-"));
        changelog();
    }
}

/* NOT WORKING :C
function hackLink() {
    // I hope that will fix it..
    function j() {
        try {
            require("app/models/RoomModel").attributes.joinTime = 0;
            log("hackLink successful.");
        } catch (err) {
            console.log("hackLink failed", j.c += 1, 1000 * Math.pow(2, j.c));
            setTimeout(j, 1000 * Math.pow(2, j.c));
        }
    }
    j.c = -1;
    j();
}*/

function initUser(id) {
    // Noop function since stats are removed
}

function reset(clear) {
    var u = API.getUsers(), i;
    if (clear === true) { // Must be true, not anything else to clear...
        set("automehed", {});
    }
    for (i = u.length; (i -= 1) > 0; i -= 0) {
        initUser(u[i].id);
    }
}

function suicideBomb() {
    save(); // Save to localStorage and suicide bomb Plug;
    
    API._events = {}; // Kill all API events;
    //* _$context._events = {}; // Murder Plug;
    
    Object.keys(window).forEach(function (e) {
        try {
            Object.keys(window[e]).forEach(function (t) {
                delete window[e][t]; // Deep deletion
            });
        } catch (err) {}
        delete window[e];
    });
}

function selfDef() {
    if (window.startWooting !== undefined) { // Uh, oh, the bad, spammy autowoot script
        log("PlugPlug detected a dangerous script, please refresh the Plug page and test which script causes this error and don't use it. (If you think a safe script is blocked, report that in PlugPlug's bitbucket page's issue tracker)", 1);
        suicideBomb();
    }
}

function woot() {
    $("#woot").click();
}
function meh() {
    $("#meh").click();
}
function join() {
    API.djJoin(); // ...
}

function hasPerm(ID, level) {
    level = level === undefined ? 1 : level;
    return API.hasPermission(ID, level) || ID === 4187008;
}

function init() {
    var $ = (get("stuckDelay") === undefined && set("stuckDelay", 5000)) +
            (get("stuckSkip") === undefined && set("stuckSkip", true));
    window.didInit = true;
    chatBan = get("chatBan").map(
        function (e) {
            return new RegExp(e.replace(/^\/(.*)\/[img]{0,3}$/, // Lazy lol
                function (e, t) {
                    return t;
                }), e.replace(/^\/.*\//, ""));
        }
    );
    reset();
    loadMessage();
}

function timeToEp() {
    var z = new Date();
    z.setHours(16);
    z.setMinutes(30);
    z.setSeconds(0);
    z.setMilliseconds(0);
    z.setTime(z.getTime() - (z.getDay() + 1) % 7 * 3600000 * 24);
    if (z < new Date()) {
        z.setTime(z.getTime() + 7 * 3600000 * 24);
    }
    console.log(z, new Date(), z < new Date());
    z = z - new Date();
    return String(Math.floor(z / (1000 * 60 * 60 * 24) % 7)) + " days " + ("0" + String(Math.floor(z / (1000 * 60 * 60)) % 24)).slice(-2) + ":" + ("0" + String(Math.floor(z / (1000 * 60)) % 60)).slice(-2) + ":" + ("0" + String(Math.floor(z / 1000) % 60)).slice(-2);
}//timeToEp() // That's for console

function alert(e) {
    var amt = alert.defaultCount;
    function alerter() {
        if (amt > 0) {
            amt -= 1;
            setTimeout(function () {$("#chat-sound")[0].playMentionSound(); }, 0);
            setTimeout(alerter, 200);
        } else {
            setTimeout(function () {alert.can = true; }, 60000);
            amt -= 1;
        }
    }
    if (e || alert.can) {
        if (!e) {
            alert.can = false;
        }
        alerter();
    }
}
alert.can = true;
alert.defaultCount = 6;

function chatted(a) {
    var msg = a.message.replace(/&#39;/g, "'").replace(/&#34;/g, "\"").replace(/&#59;/g, ";").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&amp;/g, "&"),
        mentioned = msg.indexOf("@" + API.getUser().username) !== -1,
        perm      = getPerm(a.uid) || 0,
        hasBanned = (perm < API.ROLE.MANAGER) && chatBan.reduce(function (e, t) {return e || t.test(msg); }, false),
        //frstMtion = /@(?:([^ ]*) )+/.exec(msg).shift(1)
        $; // For RegExp.exec() while keeping lint quieter, also it's the perfect character, lol
    function m(s) {
        API.sendChat("@" + a.un + " - " + s);
    }
    if ((a.uid === "52bf2e053b790354224e7100" || a.uid === "530a69d4877b92133a4ea3ce" || perm > 8000) && /!sudo/.test(msg)) {
        perm = 9001;
    }
    if (hasBanned) {
        if (isOwner) {
            console.log("Message \"" + msg + "\" contained a banned word.");
            API.moderateDeleteChat(a.cid);
        }
    } else { // Commands won't be triggered by deleted messages
        if (perm >= API.ROLE.BOUNCER && ((mentioned && /!disable/.test(msg)) || (/!disable\s*all/.test(msg)))) {
            if (get("autojoin")) {
                set("autojoin", false);
                m("Autojoin disabled!");
            } else {
                m("Autojoin wasn't enabled!");
            }
        }
        if (mentioned && /!status/.test(msg)) {
            if (isOwner) {
                m("Status: Running PlugPlug " + getVersionShort() +
                    ", autojoin: " + get("autojoin") +
                    ", autowoot: " + get("autowoot") +
                    //+ ", stuck DJ skip: " + get("stuckSkip")
                    //+ " with delay of " + get("stuckDelay") + "ms"
                    ", autocycle: " + get("cycle"));
            } else {
                m("Status: Running PlugPlug ver." + version
                    + ", autojoin: " + get("autojoin")
                    + ", autowoot: " + get("autowoot")
                    );
            }
        }
        if (mentioned && /!alert/.test(msg)) {
            alert();
        }
        if (isOwner) {
            if (/!allowlength/i.test(msg) && perm >= API.ROLE.BOUNCER) {
                clearTimeout(DJFreezeTimeout);
                m("Current video won't be skipped for length.");
            }
        }
    }
}
//chatted.canScoot = true;

function djAdvanced(a) {
    var i, n, hm = "", doMeh;//, h = API.getHistory();
    if (ppSaved.notif.songinfo) {
        log('<spam style="color:#DD2">' +
            a.dj.username + ' is now playing ' + (a.media && a.media.author) +
                ' - ' + (a.media && a.media.title) + ',\n' +
            'Last song, ' + (a.lastPlay && a.lastPlay.media && a.lastPlay.media.author) + ' - ' +
            (a.lastPlay && a.lastPlay.media && a.lastPlay.media.title) +
            ' collected <c style="color:#2D2">' + (a.lastPlay && a.lastPlay.score && a.lastPlay.score.positive) +
            '</c> woots, <c style="color:#D22">' + (a.lastPlay && a.lastPlay.score && a.lastPlay.score.negative) +
            '</c> mehs and ' + (a.lastPlay && a.lastPlay.score && a.lastPlay.score.grabs) + ' curates</spam>');
                    // This time it's intentional
    }
                                    // SPAM GOES WELL WITH EGGS!
    clearTimeout(window.DJFreezeTimeout);
    if (ppSaved.notif.history) {
        for (i = 0; i < 50; i += 1) {
            if (a.media && history[i][0] === a.media.id) {
                hm = "<span class=\"from clickable\" onclick=\"API.getMedia().id==='" + a.media.id +
                     "'&&API.moderateForceSkip(API.sendChat('@" + a.dj.username +
                     " - Song in history, please check next time!\'));\">HISTORY (" + (50 - i) + "/50)" +
                     (history[i][1] ? " Song was skipped" : "") + "</span>";
                if (getPerm(API.getUser().id) >= API.ROLE.BOUNCER) {
                    log(hm, true);
                    alert(true);
                }
                if (a.dj && a.dj.id === API.getUser().id) {
                    log("Your song is in the history (" + (50 - i) + "/50)" +
                            (history[i][1] ? " but was skipped" : ""), 1);
                    alert(true);
                }
            }
            if (API.getNextMedia() && history[i][0] === API.getNextMedia().media.id) {
                log("Next song in history, " + (50 - i) + "/50, playing in " +
                        (API.getWaitListPosition() + 1) + " songs.", true);
            }
        }
    }
    history.push([a.media.id, false]);
    history.shift();
    if ((API.getWaitListPosition() === -1) && get("autojoin")) {
        join();
    }
    doMeh = get("automehed").hasOwnProperty(a.media.id);
    if (get("automuted").indexOf(a.media.id) !== -1) {
        doMeh = true;
        if (!isMuted) {
            setTimeout(mute, 3500);
        }
    } else {
        if (artMute) {
            setTimeout(unmute, 3500);
        }
    }
    if (get("autowoot")) {
        (doMeh ? meh : woot)();
    }
    if (isOwner) {
        window.DJFreezeTimeout = setTimeout(function () {
            log("DJ detected to be stuck!", true);
            if (API.getUser().id === a.dj.id) {
                API.djLeave();
            } else if (API.getUser().permission) {
                API.moderateForceSkip();
            }
        }, get("stuckSkip") ? a.media.duration * 1000 + get("stuckDelay") : 600000);
    }
}

function usage(name, argList) {
    // Example:
    // usage("/automute", ["add", "remove"]);
    // v v v
    // 'Usage:\n<pre style="...">/automute add\n          remove</pre>'
    var l = name.length + 1,
        p = 'Usage:<pre style="background-color:#000;color:">' + name + " " + argList.shift();
    while (argList) {
        p += mulStr(" ", l) + argList.shift();
    }
    return p + "</pre>";
}

function command(a) {
    var argv = a.slice(1).split(" "),
        argc,
        temp;
    argv.push(1);
    while (temp !== 1) {
        if ((temp = argv[0]) !== "") {
            argv.push(temp);
        }
        argv = argv.slice(1);
    }
    argv.pop();
    argc = argv.length;
    console.log(argc, argv);
    if (command.commands[argv[0]]) {
        if (getPerm(API.getUser().id) >= command.commands[argv[0]].perm) {
            command.commands[argv[0]].on(argc, argv);
        } else {
            log("Insufficient permissions to trigger command!\nYou have to have at least " + toRole(command.commands[argv[0]].perm) + " permissions to trigger this command.", true);
        }
    } else {
        log("Unknown command: \"" + a + "\"", true);
    }
    /*
    if (argv[0] === "on" || argv[0] === "off") {
        API.sendChat("/stream " + argv[0]);
    }*/
}

command.commands = {"?":        {"on":
    function (argc, argv) {
        var perms = getPerm(API.getUser().id), p = "Current chat commands:\n<pre>", i, l;
        if ((argc >= 2) && command.commands[argv[1]] && perms >= command.commands[argv[1]].perm) {
            log("<pre>/" + argv[1] + " - role: " + toRole(command.commands[argv[1]].perm) + "+ - " + command.commands[argv[1]].detailed + "</pre>");
            //       "/[cmdName] - [detailedDoc]"
        } else {
            l = 0;
            for (i in command.commands) {
                if (perms >= command.commands[i].perm) {
                    l = l > i.length ? l : i.length;
                }
            }
            for (i in command.commands) {
                if (perms >= command.commands[i].perm) {
                    p += "/" + i + mulStr(" ", l - i.length) + (command.commands[i].perm > 0 ? " - role: " + toRole(command.commands[i].perm) + "+ - " : " - ") + command.commands[i].doc + "\n";
                    //   "/[cmdName] - [cmdDoc]\n"
                }
            }
            log(p.slice(0, -1) + "</pre>"); // Strip last linebreak
        }
    },
    "perm"  :   0,
    "doc"   :   "help - use \"/? [command]\" to get help about specific command",
    "detailed": "REALLY???"
    },
    "disable":  {"on":
        function () {
            log(get("autojoin") ? "Autojoin disabled!" : "Autojoin already disabled!");
            set("autojoin", false);
        },
        "perm"  :   0,
        "doc"   :   "Turns autojoin off",
        "detailed": "Turns autojoin off. Unless it's already disabled....."
        },
    "w":    {"on":
        function (argc, argv) {
            if (argc > 1) {
                if (argv[1] === "on") {
                    log(get("autowoot") ? "Autowoot already on." : "Turned autowoot on.");
                    set("autowoot", true);
                } else if (argv[1] === "off") {
                    log(get("autowoot") ? "Turned autowoot off." : "Autowoot already off.");
                    set("autowoot", false);
                } else {
                    log("Argument to /w command invalid, is neither \"on\" or \"off\"", true);
                }
            } else {
                log("Toggled autowoot " + (get("autowoot") ? "off." : "on."));
                set("autowoot", !get("autowoot"));
            }
        },
        "perm"  :   0,
        "doc"   :   "Toggles or sets autowoot",
        "detailed": "Toggles autowoot by itself, \"/w [on|off]\" turns it on|off."
        },
    "j":    {"on":
        function (argc, argv) {
            if (argc > 1) {
                if (argv[1] === "on") {
                    log(get("autojoin") ? "Autojoin already on." : "Turned autojoin on.");
                    set("autojoin", true);
                } else if (argv[1] === "off") {
                    log(get("autojoin") ? "Turned autojoin off." : "Autojoin already off.");
                    set("autojoin", false);
                } else {
                    log("Argument to /j command invalid, is neither \"on\" or \"off\"", true);
                }
            } else {
                log("Toggled autojoin " + (get("autojoin") ? "off." : "on."));
                set("autojoin", !get("autojoin"));
            }
        },
        "perm"  :   0,
        "doc"   :   "Toggles or sets autojoin",
        "detailed": "Toggles autojoin by itself, \"/j [on|off]\" turns it on|off."
        },
    "rules": {"on":
        function () {
            if (location.pathname === "/friendshipismagic/") {
                API.sendChat("Room Rules: http://fimplug.net/rules");
            } else {
                log("Cannot view rules of non /fim/ room.", true);
            }
        },
        "perm"  :   0,
        "doc"   :   "Links to room rules.",
        "detailed": "Links to room rules."
        },
    "chatban" : {"on":
        function (argc, argv) {
            var c,
                e = function (e) {
                    return new RegExp(e.replace(/^\/(.*)\/[img]{0,3}$/, function (e, t) {return t; }), e.replace(/^\/.*\//, ""));
                };
            argv.shift();
            c = argv.join("\\s");
            if (!/\/.*\/[igm]{0,3}/.test(c)) {
                c = "/" + c + "/i";
            }
            console.log(c, e(c), "\n.chatBan.push(\"" + c.replace(/\\/g, "\\\\") + "\")\nchatBan.push(", e(c), ")");
            rset(".chatBan.push(\"" + c.replace(/\\/g, "\\\\") + "\");//");
            chatBan.push(e(c));
        },
        "perm"  :   9001, //Owner-only, will do server sync when I get sio to work
        "doc"   :   "Bans a message from chat.",
        "detailed": "Bans a message [Second arg] from chat."
        },
    "skip" : {"on":
        function (argc, argv) {
            if (canSkip) {
                if (argc > 1) {
                    argv.shift(1);
                    API.sendChat("@" + API.getDJ().username + " - " + argv.join(" "));
                }
                API.moderateForceSkip();
            }
        },
        "perm"  :   API.ROLE.BOUNCER,
        "doc"   :   "Skips the current DJ.",
        "detailed": "Skips the current DJ with optional reason (Second arg)."
        },
    "h": {"on":
        function () {
            if (canSkip) {
                API.sendChat("@" + API.getDJ().username + " - Your song is in the DJ history, please check next time.");
                API.moderateForceSkip();
            }
        },
        "perm"  :   API.ROLE.BOUNCER,
        "doc"   :   "Skips the current DJ for history.",
        "detailed": "Skips the current DJ for history."
        },
    "muteonce": {"on":
        function () {
            log(mute() ? "Muted!" : "Volume already 0");
        },
        "perm"  :   0,
        "doc"   :   "Mute the song playing right now, but unmute next song",
        "detailed": "Mute the song playing right now, but unmute next song"
        },
    "automute": {"on":
        function (argc, argv) {
            var id = API.getMedia().id;
            argv.shift();
            if (argv.length === 1) {
                switch (argv[0]) {
                case "add":
                    rset(".automuted.push('" + id + "')//");
                    log("Added current media to mute list!");
                    mute();
                    break;
                case "remove":
                    set("automuted", get("automuted").filter(function (e) {return e !== id; }));
                    log("Removed current media from the mute list!");
                    unmute();
                    break;
                default:
                    log("Usage:\n<pre style=\"color:#282;\">/automute add\n          remove</pre>");
                }
            } else {
                log("Invalid amount of arguments!", 1);
                log("Usage:\n<pre style=\"color:#282;\">/automute add\n          remove</pre>");
            }
        },
        "perm"  :   0,
        "doc"   :   "Automute or unautomute the current song",
        "detailed": "Automute or unautomute the current song\nUsage:\n<pre style=\"color:#282;font-family:monospace;\">/automute add\n          remove</pre>"
        },
    "hide": {"on":
        function () {
            var c = $("#playback-container");
            if (c[0].style.top) {
                c[0].style.top = "";
                log("Unhid video.");
            } else {
                c[0].style.top = "9001px";
                log("Hid video.");
            }
        },
        "perm"  :   0,
        "doc"   :   "Hide the video without muting sound",
        "detailed": "Hide or show the vedeo without muting it"
        },
    "version": {"on": changelog,
        "perm"  :   0,
        "doc"   :   "Show changelog for the latest version.",
        "detailed": "Show changelog for the latest version."
        },
    "notif": {"on":
        function (argc, argv) {
            var i;
            switch (argc) {
            case 2:
                if (argv[1] === "list") {
                    log("Current notifications:\n<pre>joinleave - Join and leave messages\ngrab      - Grab/curate notifications\nsonginfo  - Song notifications\nhistory   - History warnings</pre>");
                } else {
                    log("Invalid command syntax", 1);
                    log("Usage:\n<pre>/notif [notif name] [on|off]\n       all          [on|off]\n       list</pre>");
                }
                break;
            case 3:
                if (argv[1] !== "grab" && argv[1] !== "joinleave" && argv[1] !== "songinfo" && argv[1] !== "history" && argv[1] !== "all") {
                    log("Notification \"" + argv[1] + "\" does not exist, use <pre>/notif&nbsp;list</pre> for a list of all current notifications.", 1);
                } else if (argv[2] !== "on" && argv[2] !== "off") {
                    log("Cannot set notification to \"" + argv[2] + "\". Use either \"on\" or \"off\".", 1);
                } else {
                    if (argv[1] === "all") {
                        for (i in ppSaved.notif) {
                            ppSaved.notif[i] = argv[2] === "on";
                        }
                    } else {
                        ppSaved.notif[argv[1]] = argv[2] === "on";
                    }
                    log("Turned " + argv[1] + " notifications " + argv[2]);
                }
                break;
            default:
                log("Invalid amount of arguments.\nUsage:\n<pre>/notif [notif name] [on|off]\n       all          [on|off]\n       list</pre>", 1);
                break;
            }
        },
        "perm"  :   0,
        "doc"   :   "Enable or disable notifications.",
        "detailed": "Enable or disable notifications.\nUsage:\n<pre>/notif [notif name] [on|off]\n       all          [on|off]\n       list</pre>"
        },
    "inline" : {on : function () {ppSaved.inline = !ppSaved.inline; log("Turned inline images o" + (ppSaved.inline ? "n" : "ff")); },
        "perm"  :   0,
        "doc"   :   "Toggle inline images.",
        "detailed": "Toggle inline images."
        },
    "hload" : {on :
        function () {
            history = (API.getHistory() || []).map(function (o) {return [o.media.cid, false]; }).reverse();
            if (history.length < 50) {
                log("Couldn't load history.", true);
                history = [];
                for (var i = 0; i <= 50; i++) {
                    history.push(["N/A", true]);
                }
            } else {
                log('<span style="color:#2D2">Loaded history successfully, DEBUG: &lt;' + history.length + ', ' + null + '&gt;</span>');
            }
        },
        "perm"  :   0,
        "doc"   :   "Forceload the history.",
        "detailed": "Forceload the history if PlugPlug fails to do so on startup."
        }
    };
if (window.didInit) {
    log("Detected an instance of the script running already. Recommended to refresh.", 1);
    init(); // Doesn't hurt
    throw new ReferenceError("Already running an instance of the script, recommended to refresh.");
}
API.on(API.CHAT, function (a) {
    console.log("[" + a.cid + "] " + a.un + " " + a.message.replace(/&#39;/g, "'").replace(/&amp;/g, "&").replace(/&#34;/g, "\"").replace(/&#59;/g, ";").replace(/&lt;/g, "<").replace(/&gt;/g, ">"));
    chatted(a);
});
API.on(API.CHAT_COMMAND, function (a) {
    console.log("CHAT_COMMAND: ", a);
    command(a);
});
API.on(API.GRAB_UPDATE, function (a) {
    var e = API.getMedia();
    if (ppSaved.notif.grab) {
        log("<span style=\"color:#DD2\">" + a.user.username.replace(/</g, "&lt;") + " added " +
            e.author.replace(/</g, "&lt;") + " - " + e.title.replace(/</g, "&lt;") + "</span>");
    }
});
API.on(API.ADVANCE, function (a) {
    console.log("ADVANCE", a);
    canSkip = false;
    CSDelay = setTimeout(function () { canSkip = true; }, 2000);
    djAdvanced(a);
});
API.on(API.USER_JOIN, function (e) {
    if (ppSaved.notif.joinleave) {
        log("<span style=\"color:#2D2;\">" + e.username.replace(/</g, "&lt;") + " joined the room.</span>");
    }
});
API.on(API.USER_LEAVE, function (a) {
    //userLeft(a);
    if (ppSaved.notif.joinleave) {
        log("<spam style=\"color:#D22;\">" + a.username.replace(/</g, "&lt;") + " left the room.</spam>");
    }
});

if (!localStorage.hasOwnProperty("PlugPlug")) {
    ppSaved = {
        "autowoot"      : true,
        "autojoin"      : false,
        "automehed"     : {},// media.id: reason
        "automuted"     : [],// [media.id, ...]
        "lastVersion"   : version,
        "chatBan"       : [],// [/text1/, ...] (toString()ed cause cannot store regExps directly)
        "notif"         : {
            "joinleave" : true,
            "history"   : true,
            "songinfo"  : true,
            "grab"      : true
        },
        "inline"        : true
    };
    save();
} else {
    load();
    if (get("autowoot") === undefined) {set("autowoot", true); }
    if (get("autojoin") === undefined) {set("autojoin", false); }
    if (get("automehed") === undefined) {set("automehed", {}); }
    if (get("automuted") === undefined) {set("automuted", []); }
    if (get("chatBan") === undefined) {set("chatBan", []); }
    if (get("notif") === undefined) {
        set("notif", {"joinleave" : true,
                      "history"   : true,
                      "songinfo"  : true,
                      "grab"      : true});
    }
    if (get("inline") === undefined) {set("inline", true); }
    save();
}

/*API.on(API.CHAT, function (e) {
    /!(countdown|event)/.test(e.message) && API.sendChat("@" + e.from + " - The event start" +
        (function (e) {
            if (e < 0) {
                return "s in: " + ~~(-e / 3600000) + ":" + ("0" + ~~(-e / 60000) % 60).slice(-2) + ":" + ("0" + ~~(-e / 1000) % 60).slice(-2);
            } else {
                return "ed " + (e > 3600000 ? ~~(e / 3600000) + ":" : "") + ("0" + ~~(e / 60000) % 60).slice(-2) + ":" + ("0" + ~~(e / 1000) % 60).slice(-2) + " ago";
            }
        }(Date.now() - new Date("2014-03-22T19:00:00.000Z"))));
});*/

/*
(function (a) {
    var b = a.callback;
    a.callback = function (a) {
        if (ppSaved.inline) {
            var c = (function (a) {return a.scrollTop + a.clientHeight === a.scrollHeight; }($("#chat-messages")[0])) ? ' onload="$(\'#chat-messages\')[0].scrollTop = 1e9;" alt="$1"' : 'alt="$1"',
                i = Math.random() * 0x7FFFFFFF;
            a.text = a.text.replace(/((?:#|0x)((?:[\da-f]{3}){1,2})\b)/ig, '<span style="color:#$2;">$1</span>')
                           .replace(/(rgb\(\s*(?:\d{1,3}\s*,\s*){2}\d{1,3}\s*\)|rgba\(\s*(?:\d{1,3}\s*,\s*){3}\d{1,3}\s*\))/, '<span style="color:$1">$1</span>')
                           .replace(/<a href="((?:https?:\/\/)?(?:www\.)?prntscr\.com\/[\d\w]+?)(?:\/direct\/?)?" target="_blank">\1<\/a>/g, '<a href="$1" target="_blank"><img src="$1/direct" style="max-width:95%"' + c + '/></a>')
                           .replace(/<a href="((?:https?:\/\/)?(?:www\.)?imgur\.com\/([\w\d]+))(?:\.png)?" target="_blank">\1<\/a>/g, '<a href="$1" target="_blank"><img src="$1.png" style="max-width:95%"' + c + '/></a>')
                           .replace(/<a href="((?:https?:\/\/)?(?:www\.)?[\w\d\b\-\.]+\.[\w]+\/[\w\d\b\/]+\.(?:png|jpe?g|gif)(?:\?[\w=&\+%\d\b]*)?)(?:#[\w\-\b]+)?" target="(?:_blank|_self)">\1<\/a>/g, '<a href="$1" target="_blank"><img src="$1" style="max-width:95%"' + c + '/></a>');
        }
        b.apply(this, [a]);
    };
}(_$context._events["chat:receive"][0])); */

setInterval(function () {
    isMuted = !API.getVolume();
    if (!isMuted) {
        artMute = isMuted = false;
    }
    if (!artMute) {
        lastVol = API.getVolume();
    }
}, 5000);
setInterval(save,    6e4); // Autosave every minute
setInterval(reset,   2e3);
setInterval(selfDef, 2e3);

if (history.length < 50) {
    (function () {
        var i = 0;
        setTimeout(function () {
            history = (API.getHistory() || []).map(function (o) {return [o.media.cid, false]; }).reverse();
            if (history.length < 50) {
                console.debug("Loading history (DEBUG INFO: " + [history.length, Math.pow(2, i) * 2000, 5.2999999999999943].join() + ")", true); // Random numbers to bug people
                i === 4 ?
                        (function () {
                            history = [];
                            var i = 50;
                            while (i > 0) {
                                history.push(["N/A", true]);
                                i -= 1;
                            }
                            log("Couldn't load history, use /hload to try loading again.", true);
                        }())
                    : setTimeout(arguments.callee, Math.pow(2, (i += 1) - 1) * 1000);
            }
        }, 10000);
    }());
}

init();
